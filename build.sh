#!/bin/sh

cd $(dirname $0)

set -ex
pushd rpn-wasm
if [ "$1" = rel ]; then
    wasm-pack build --target web --release
else
    wasm-pack build --target web --dev
fi
popd

mkdir -p public/wasm
cp rpn-wasm/pkg/rpn{.js,_bg.wasm} public/wasm
