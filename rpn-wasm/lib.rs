use std::f64;
use std::collections::VecDeque;
use std::rc::Rc;
use std::cell::RefCell;

use wasm_bindgen::prelude::*;
use wasm_bindgen::JsCast;

use web_sys::{CanvasRenderingContext2d, CssStyleDeclaration, Document, HtmlElement
    , HtmlCanvasElement, HtmlInputElement, Window};
use web_sys::console;

const BOX_SIZE: f64 = 64.0;
const BOX_PADDING: f64 = 3.0; // must be odd number
const MAX_OP: u32 = 5;
const MAX_NUM: u32 = 6;
const INPUT_BOXES: u32 = 3;
const MAX_INPUT: i32 = 999;
const MIN_INPUT: i32 = -999;
const MAX_NUM_BOXES: u32 = MAX_OP + MAX_NUM + INPUT_BOXES;
const NUM_ROWS: u32 = 8;
const WIDTH: u32 = MAX_NUM_BOXES * (BOX_SIZE + BOX_PADDING) as u32 + BOX_PADDING as u32;
const HEIGHT: u32 = NUM_ROWS * (BOX_SIZE + BOX_PADDING) as u32 + BOX_PADDING as u32;
const TEXT_OFFX: f64 = (MAX_OP + MAX_NUM) as f64 * (BOX_SIZE + BOX_PADDING) + 10.0;
const TEXT_OFFY: f64 = 6.0;
const HEIGHTF: f64 = HEIGHT as f64;
const WIDTHF: f64 = WIDTH as f64;
const STARTING_DROP_SPEED: f64 = 0.1;
const MIN_EQUATION_INTEVAL: f64 = 2000.0;
const STARTING_EQUATION_INTERVAL: f64 = 20000.0;


// hack to avoid making more JsValues than needed
struct Colors {
    background: JsValue,
    gridline: JsValue,
    prefix_eq: JsValue,
    postfix_eq: JsValue,
    text: JsValue,
    failtext: JsValue,
}

const BACKGROUND_COLOR: &'static str = "#dddddd";

impl Colors {
    fn new() -> Colors {
        Colors {
            background: jstr(BACKGROUND_COLOR),
            gridline: jstr("#bdbdbd"),
            prefix_eq: jstr("#c0572a"),
            postfix_eq: jstr("#6e7a00"),
            text: jstr("#3b2f2b"),
            failtext: jstr("#510101"),
        }
    }
}

struct TextInput {
    elem: HtmlInputElement,
    style: CssStyleDeclaration,
}

impl TextInput {
    fn set_pos(&self, posx: f64, posy: f64) {
        self.style.set_property("left", &format!("{posx}px")).unwrap();
        self.style.set_property("top", &format!("{posy}px")).unwrap();
    }
}

fn make_input(doc: &Document, posx: f64, posy: f64) -> Result<TextInput, JsValue> {
    let txt = doc.create_element("input")?;
    txt.set_attribute("type", "text")?;
    let txt_style = txt.dyn_ref::<HtmlElement>().unwrap().style();
    let txt = TextInput {
        elem: txt.dyn_into().unwrap(),
        style: txt_style,
    };
    txt.style.set_property("position", "absolute")?;
    txt.style.set_property("backgound", BACKGROUND_COLOR)?;
    txt.style.set_property("font", "46px serif")?;
    txt.style.set_property("width", "192px")?;
    txt.set_pos(posx, posy);
    doc.body().unwrap().append_child(&txt.elem)?;

    Ok(txt)
}

fn js_rand_range(min: i32, max: i32) -> i32 {
    min + (js_sys::Math::random() * ((max - min) as f64 + 1.0)) as i32
}

const OP_ADD: char = '+';
const OP_SUB: char = '-';
const OP_MUL: char = '*';
const OP_DIV: char = '/';

fn rand_op() -> char {
    // TODO enable division
    match js_rand_range(0, 3) {
        0 => OP_ADD,
        1 => OP_SUB,
        2 => OP_MUL,
        3 => OP_DIV,
        _ => unreachable!(),
    }
}

fn document() -> Document {
    let win = web_sys::window().expect("failed to get window");
    win.document().expect("failed to get document")
}

struct Equation {
    solution: i32,
    op: Vec<char>,
    height: f64,
    is_postfix: bool, // false if prefix
    input: TextInput,
}

impl Equation {
    fn random() -> Equation {
        // TODO: imporove RNG
        let num_op = js_rand_range(1, 5);
        let num_num = num_op + 1;
        let is_postfix = js_rand_range(0, 1) == 0;
        let mut solution = MAX_INPUT + 1;
        let mut op = Vec::new();
        let mut eval_stack = Vec::new();
        while solution > MAX_INPUT || solution < MIN_INPUT {
            op.clear();
            for _ in 0.. num_num {
            //while op.len() < 2 {
                let i = js_rand_range(1, 9);
                eval_stack.push(i);
                let ic = char::from_digit(i as u32, 10).unwrap();
                op.push(ic);
                //num_num -= 1;
            }
            for _ in 0..num_op {
                let lhs = eval_stack.pop().unwrap();
                let rhs = eval_stack.pop().unwrap();
                match rand_op() {
                    OP_ADD => {
                        eval_stack.push(lhs + rhs);
                        op.push(OP_ADD);
                    },
                    OP_SUB => {
                        eval_stack.push(rhs - lhs);
                        op.push(OP_SUB);
                    },
                    OP_MUL => {
                        eval_stack.push(lhs * rhs);
                        op.push(OP_MUL);
                    },
                    OP_DIV => if lhs != 0 && rhs % lhs == 0 {
                        eval_stack.push(rhs/lhs);
                        op.push(OP_DIV);
                    } else {
                        eval_stack.push(rhs+lhs);
                        op.push(OP_ADD);
                    } /* todo!("division") */,
                    _ => unreachable!(),
                }
            }

            /*
            while num_op > 0 || num_num > 0 {
                if (num_op < 2 && num_num > 0) || (num_op > 0 && eval_stack.len() < 2) || (js_rand_range(0, 1) == 0 && num_num > 0) {
                    assert!(num_num > 0, "tried to generate too many numbers");
                    // number
                    let i = js_rand_range(1, 9);
                    eval_stack.push(i);
                    let ic = char::from_digit(i as u32, 10).unwrap();
                    op.push(ic);
                    num_num -= 1;
                } else {
                    // op
                    let lhs = eval_stack.pop().unwrap();
                    let rhs = eval_stack.pop().unwrap();
                    match rand_op() {
                        OP_ADD => {
                            eval_stack.push(lhs + rhs);
                            op.push(OP_ADD);
                        },
                        OP_SUB => {
                            eval_stack.push(rhs - lhs);
                            op.push(OP_SUB);
                        },
                        OP_MUL => {
                            eval_stack.push(lhs * rhs);
                            op.push(OP_MUL);
                        },
                        OP_DIV => /* if rhs % lhs == 0 {
                            eval_stack.push(rhs/lhs);
                            op.push(OP_DIV);
                        } else {
                            eval_stack.push(rhs+lhs);
                            op.push(OP_ADD);
                        } */ todo!("division"),
                        _ => unreachable!(),
                    }
                    num_op -= 1;
                }
            }

            assert!(num_op < 1, "faild to generate op");
            assert!(num_num < 1, "faild to generate number");
            assert!(eval_stack.len() == 1, "eval stack is not empty");

            */
            solution = eval_stack.pop().unwrap();
        }

        if !is_postfix {
            op.reverse();
        }
        let doc = document();
        return Equation { solution, op, height: -64.0, is_postfix, input: make_input(&doc, TEXT_OFFX, 0.0).unwrap() };
    }
}

struct Ctx {
    window: Window,
    equations: VecDeque<Equation>,
    last_frame: f64,
    last_equation: f64,
    current_time: f64,
    drop_speed: f64,
    equation_interval: f64,
    score: u32,
    finished: bool,
}

// Called when the wasm module is instantiated
#[wasm_bindgen(start)]
pub fn main() -> Result<(), JsValue> {
    std::panic::set_hook(Box::new(panic));
    let colors = Colors::new();
    // Use `web_sys`'s global `window` function to get a handle on the global
    // window object.
    let window = web_sys::window().expect("no global `window` exists");
    let document = window.document().expect("should have a document on window");

    let canvas = document.get_element_by_id("game").ok_or("faild to find #game")?
        .dyn_into::<HtmlCanvasElement>().unwrap();
    canvas.set_width(WIDTH);
    canvas.set_height(HEIGHT);

    let g = Rc::new(canvas.get_context("2d")?.unwrap()
        .dyn_into::<CanvasRenderingContext2d>().unwrap());
    g.set_font("64px serif");
    g.set_text_align("center");

    let ctx = Rc::new(RefCell::new(Ctx {
        equations: VecDeque::new(),
        last_frame: 0.0,
        window,
        drop_speed: STARTING_DROP_SPEED,
        equation_interval: STARTING_EQUATION_INTERVAL,
        last_equation: 0.0,
        current_time: 0.0,
        score: 0,
        finished: false,
    }));
    ctx.borrow_mut().equations.push_back(Equation::random());
    //ctx.borrow_mut().equations.push_back(Equation::random());
    //ctx.borrow_mut().equations.push_back(Equation::random());
    //ctx.borrow_mut().equations.push_back(Equation::random());
    //ctx.borrow_mut().equations.push_back(Equation::random());

    {
        // HACK to work around rust's lifetime restrictions
        let f: Rc<RefCell<Option<Closure<dyn Fn(f64)>>>> = Rc::new(RefCell::new(None));
        let f2 = f.clone();
        let ctx_clone = ctx.clone();

        *f.borrow_mut() = Some(Closure::<dyn Fn(f64)>::new(move |newtime| {
            let this = f2.clone();
            let g = g.clone();
            let ctx = ctx_clone.clone();
            let mut ctx_ref = ctx.borrow_mut();
            let dt = newtime - ctx_ref.last_frame;
            ctx_ref.current_time = newtime;
            update(&colors, &g, &mut ctx_ref, dt);
            ctx_ref.last_frame = newtime;
            ctx_ref.window.request_animation_frame(
                this.borrow().as_ref().unwrap().as_ref().unchecked_ref())
                .expect("failed to request new animation");
        }));

        ctx.borrow().window.request_animation_frame(f.borrow().as_ref().unwrap().as_ref().unchecked_ref())?;
    }

    document
        .get_element_by_id("loading").ok_or("faild to find #loading element")?
        .dyn_ref::<HtmlElement>().unwrap()
        .style()
        .set_property("display", "none")?;

    Ok(())
}

fn update(colors: &Colors, g: &CanvasRenderingContext2d, ctx: &mut Ctx, dt: f64) {
    if ctx.finished {
        draw_frame(colors, g, ctx);
        return;
    }
    //console::log_2(&jstr("dt: "), &JsValue::from_f64(dt));
    if ctx.current_time - ctx.last_equation > ctx.equation_interval {
        ctx.last_equation = ctx.current_time;
        ctx.equations.push_front(Equation::random());
        ctx.equation_interval -= 100.0;
        if ctx.equation_interval < MIN_EQUATION_INTEVAL {
            ctx.equation_interval = MIN_EQUATION_INTEVAL;
        }

        if ctx.equations.len() as u32 > NUM_ROWS {
            ctx.finished = true;
        }
    }

    let mut i = 0;
    while i < ctx.equations.len() {
        let txt = ctx.equations[i].input.elem.value();
        if let Ok(input) = txt.parse::<i32>() {
            if input == ctx.equations[i].solution {
                ctx.score += ctx.equations[i].op.len() as u32;
                ctx.equations[i].input.elem.remove();
                ctx.equations.remove(i);

                if ctx.equations.is_empty() {
                    ctx.equations.push_front(Equation::random());
                }
                continue;
            }
        }

        ctx.equations[i].height += ctx.drop_speed * dt;
        ctx.equations[i].height = f64::clamp(ctx.equations[i].height, 0.0, HEIGHTF - BOX_SIZE);
        let next = ctx.equations.get(i + 1);
        if next.is_some() {
            let old_height = ctx.equations[i].height;
            let next = next.unwrap();
            let next_height = next.height;
            drop(next);
            if old_height > next_height - BOX_SIZE - BOX_PADDING {
                ctx.equations[i].height = next_height - BOX_SIZE - BOX_PADDING;
            }
        }
        ctx.equations[i].input.set_pos(TEXT_OFFX, ctx.equations[i].height + TEXT_OFFY);
        i += 1;
    }

    draw_frame(colors, g, ctx);
}

fn draw_frame(colors: &Colors, g: &CanvasRenderingContext2d, ctx: &Ctx) {
    g.set_fill_style(&colors.background);
    g.fill_rect(0.0, 0.0, WIDTH as f64, HEIGHT as f64);

    g.set_stroke_style(&colors.gridline);
    for i in 1..(MAX_OP + MAX_NUM + 1) {
        let off = (BOX_SIZE + BOX_PADDING) * i as f64 - 1.0;
        g.begin_path();
        g.move_to(off, 0.0);
        g.line_to(off, HEIGHTF);
        g.stroke();
    }

    g.set_font("64px serif");
    for e in &ctx.equations {
        let mut off = f64::floor(BOX_PADDING / 2.0);
        for c in &e.op {
            if e.is_postfix {
                g.set_fill_style(&colors.postfix_eq);
            } else {
                g.set_fill_style(&colors.prefix_eq);
            }
            g.fill_rect(off, e.height, BOX_SIZE, BOX_SIZE);
            g.set_fill_style(&colors.text);
            let mut tmpbuf = [0u8; 6];
            const FONT_OFF: f64 = 7.0;
            g.fill_text(c.encode_utf8(&mut tmpbuf), off + BOX_SIZE/2.0, e.height + BOX_SIZE - FONT_OFF).unwrap();
            off += BOX_SIZE + BOX_PADDING;
        }
    }

    g.set_fill_style(&colors.text);

    // FORMAT
    g.set_font("20px serif");
    g.fill_text(&format!("Score: {}", ctx.score), WIDTHF - 60.0, 30.0).unwrap();

    if ctx.finished {
        g.set_fill_style(&colors.failtext);
        g.set_font("80px serif");
        let t = "GAME OVER";
        g.fill_text(t, WIDTHF/2.0, 300.0).unwrap();

        g.set_font("40px serif");
        let t = "REFRESH TO TRY AGAIN";
        g.fill_text(t, WIDTHF/2.0, 390.0).unwrap();

        g.fill_text(&format!("Score: {}", ctx.score), WIDTHF/2.0, 430.0).unwrap();
    }
}

#[inline]
fn jstr(s: &str) -> JsValue {
    JsValue::from_str(s)
}

fn panic(info: &std::panic::PanicInfo) {
    console::error_1(&jstr(&format!("{}", info)));

    std::process::exit(1);
}
